CREATE TABLE account
(
    user_key int         NOT NULL PRIMARY KEY UNIQUE,
    username varchar(25) NOT NULL,
    csv      int,
    constraint csv
        foreign key (csv) references csv (id)
);



CREATE TABLE column_name
(
    id   int auto_increment primary key not null,
    name varchar(25)
);

CREATE TABLE csv
(
    id             int,
    id_column_name int,
    constraint csv_pk
        primary key (id, id_column_name),
    constraint id_column_name_fk
        foreign key (id_column_name) references column_name (id)
)