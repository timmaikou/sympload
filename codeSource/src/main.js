import config from "../../config.js"
import readLine from "readline-sync"

import {
    getSubFromFootPatrol,
    getSubFromHtmlTestPage,
    getSubFromJo,
    getSubFromStarcow
} from "./service/transformerCSVtoObject.js"
import {
    launchPupForFootPatrol,
    launchPupForHtmlTestPage,
    launchPupForJo,
    launchPupForStarcow
} from "./service/launchPup.js"
import {addFieldsForFootPatrol, addFieldsForHtmlTestPage, addFieldsForStarcow} from "./service/sendMessageOnDiscord.js"
import {FileLinkToWebSite} from "./object/FileLinkToWebSite.js"
import {scrapDataFromCsv} from "./service/scrapping.js"
import {getListOfSites} from "./service/getListOfSites.js"

const HTML_PAGE = new FileLinkToWebSite("testCsvFile", launchPupForHtmlTestPage, config.HTML_PAGE, getSubFromHtmlTestPage, addFieldsForHtmlTestPage, "https://www.starcowparis.com/themes/starcow/assets/img/Logo_starcow.png")
const STARCOW = new FileLinkToWebSite("starcow", launchPupForStarcow, config.STARCOW, getSubFromStarcow, addFieldsForStarcow, "https://www.starcowparis.com/themes/starcow/assets/img/Logo_starcow.png")
const FOOT_PATROL = new FileLinkToWebSite("info_footpatrol", launchPupForFootPatrol, config.FOOT_PATROL, getSubFromFootPatrol, addFieldsForFootPatrol, "https://i0.wp.com/thesneakersbible.fr/wp-content/uploads/2021/04/logo-footpatrol-paris.jpeg?resize=770%2C770&ssl=1")
const JO = new FileLinkToWebSite("jo", launchPupForJo, config.JO, getSubFromJo, addFieldsForFootPatrol, "https://upload.wikimedia.org/wikipedia/fr/thumb/6/68/Logo_JO_d%27%C3%A9t%C3%A9_-_Paris_2024.svg/1200px-Logo_JO_d%27%C3%A9t%C3%A9_-_Paris_2024.svg.png")
const SITES = [HTML_PAGE, STARCOW, FOOT_PATROL, JO]

main()

async function main() {
    const listOfSites = getListOfSites(SITES)
    const idOfSiteToLaunch = readLine.question(listOfSites)

    await scrapDataFromCsv(SITES[idOfSiteToLaunch], config)
}