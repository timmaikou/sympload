const PATH_OF_CSV = './resources/'

export class FileLinkToWebSite {
    name
    pupFunction
    csvToObjectFunction
    discordMessageFunction
    url
    logoLinkToWebsite

    constructor(name, pupFunction, url, csvToObjectFunction, discordLMessageFunction, logoLinkToWebsite) {
        this.name = name
        this.pupFunction = pupFunction
        this.url = url
        this.csvToObjectFunction = csvToObjectFunction
        this.discordMessageFunction = discordLMessageFunction
        this.logoLinkToWebsite = logoLinkToWebsite
    }

    nameWithExtension() {
        return this.name + ".csv"
    }

    pathOfCsv() {
        return PATH_OF_CSV + this.nameWithExtension()
    }
}