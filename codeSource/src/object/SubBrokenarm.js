class SubBrokenarm {
    name;
    firstName;
    day;
    month;
    year;
    phoneNumber;
    email;
    country;
    size;

    constructor(name, firstName, day, month, year, phoneNumber, email, country, size) {
        this.name = name;
        this.firstName = firstName;
        this.day = day;
        this.month = month;
        this.year = year;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.country = country;
        this.size = size;
    }

}

