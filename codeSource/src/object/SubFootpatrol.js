export class SubFootpatrol {
    fullName
    email
    dayOfBirthday
    monthOfBirthday
    yearOfBirthday
    instagram
    phoneNumber
    city
    postalCode
    sizeUE
    fullBirthday

    constructor(fullName, email, dayOfBirthday, monthOfBirthday, yearOfBirthday, instagram, phoneNumber, city, postalCode,sizeUE, fullBirthday) {
        this.fullName = fullName
        this.email = email
        this.dayOfBirthday = dayOfBirthday
        this.monthOfBirthday = monthOfBirthday
        this.yearOfBirthday = yearOfBirthday
        this.instagram = instagram
        this.phoneNumber = phoneNumber
        this.city = city
        this.postalCode = postalCode
        this.sizeUE = sizeUE
        this.fullBirthday = fullBirthday
    }
}