export class SubOpium {
    firstName;
    lastName;
    email;
    password;
    fullBirthday;
    size;

    constructor(firstName, lastName, email, password, fullBirthday, size) {
        this.firstName = firstName;
        this.lastName = lastName
        this.email = email;
        this.password = password;
        this.fullBirthday = fullBirthday;
        this.size = size;
    }
}
