export class SubStarcow {
    lastName;
    firstName;
    email;
    address;
    postalCode;
    city;
    country;
    phoneNumber;
    instagram;
    size;

    constructor(lastName, firstName, email, address, postalCode, city, country, phoneNumber, instagram, size) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
        this.country = country;
        this.size = size;
        this.phoneNumber = phoneNumber;
        this.instagram = instagram;
    }
}
