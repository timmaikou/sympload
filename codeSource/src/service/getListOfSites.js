const END_LINE = '\n'

export function getListOfSites(SITES) {
    let sitesToLaunch = "Quel site voulez vous lancer ?" + END_LINE
    for (let i = 0; i < SITES.length; i++) {
        sitesToLaunch += "    " + i + " : " + SITES[i].name + END_LINE
    }
    return sitesToLaunch
}