import fs from "fs";


export function getProxies() {
    return new Promise((resolve, reject) => {
        fs.readFile('./resources/proxies.txt', (err, data) => {
            if (err) throw err;

            let proxiesSplit = data.toString().split('\n');
            resolve(proxiesSplit)
        });
    })
}