import puppeteer from "puppeteer"

let totalOpenedbrowser = 0
const minBrowser = 2
const wait = () => new Promise((resolve) => setTimeout(resolve, 200))

export async function launchPup(subscription, site, proxy, config) {
    while (totalOpenedbrowser >= minBrowser)
        await wait()
    totalOpenedbrowser++

    puppeteer.launch({headless: false, slowMo: 0}).then(async browser => {
        const page = await browser.newPage()

        await page.goto(site.url)
        await page.waitForNavigation()
        await site.pupFunction(subscription, page)
        await page.close()
        await browser.close()
        totalOpenedbrowser--
    })
}

export async function launchPupForStarcow(subscription, page) {
    await page.keyboard.press("Tab")
    await page.keyboard.press("Tab")
    await page.keyboard.press("Tab")
    await page.keyboard.type(subscription.lastName)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.firstName)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.email)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.address)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.postalCode)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.city)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.country)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.phoneNumber)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.instagram)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.size.toString())

    await page.keyboard.press('Tab')
    await page.keyboard.press('Enter')
}

export async function launchPupForFootPatrol(subscription, page) {
    await page.keyboard.press("Enter")
    await page.keyboard.press("Tab")
    // fullName
    await page.keyboard.type(subscription.fullName)
    await delay(1000)
    await page.keyboard.press('Enter')

    // email
    await delay(1000)
    await page.keyboard.type(subscription.email)
    await page.keyboard.press('Enter')
    await delay(1000)
    await page.keyboard.press('Tab')

    // instagram
    await delay(1000)
    await page.keyboard.type(subscription.instagram)
    await page.keyboard.press('Enter')
    await delay(1000)
    await page.keyboard.press('Tab')

    // birthday
    await delay(1000)
    await page.keyboard.type(subscription.dayOfBirthday)
    await page.keyboard.press('Tab')
    await delay(1000)
    await page.keyboard.type(subscription.monthOfBirthday)
    await page.keyboard.press('Tab')
    await delay(1000)
    await page.keyboard.type(subscription.yearOfBirthday)
    await page.keyboard.press('Enter')

    // phoneNumber
    await delay(1000)
    await page.keyboard.type(subscription.phoneNumber)
    await page.keyboard.press('Enter')

    // city
    await page.keyboard.type(subscription.city)
    await delay(1000)
    await page.keyboard.press('Enter')

    // postalCode
    await page.keyboard.type(subscription.postalCode)
    await delay(1000)
    await page.keyboard.press('Enter')

    // sizeUE
    await page.keyboard.type(subscription.sizeUE)
    await delay(1000)

    await page.keyboard.press('ArrowDown')
    await delay(500)
    await page.keyboard.press('Enter')
    await delay(500)
    await page.keyboard.press('a')
    await delay(500)
    await page.keyboard.press('a')
    await delay(500)
    await page.keyboard.press('a')
    await delay(500)
    await page.keyboard.press('Tab')
    await page.keyboard.press('Enter')
}

export async function launchPupForHtmlTestPage(subscription, page) {
    const dicoSelector = {
       name: '#name',
       mail: '#mail',
       submit: '#submit'
    }
    await type(page,dicoSelector['name'], subscription.name)
    await type(page, dicoSelector['mail'], subscription.email)
    await click(page, dicoSelector['submit'])
}

export async function launchPupForJdSport(subscription, page) {
    await page.keyboard.press('Tab')
    await page.keyboard.type('Nom')
    await page.keyboard.press('Tab')
    await page.keyboard.type('PrÃ©nom')
    await page.keyboard.press('Tab')
    await page.keyboard.type('email')
    await page.keyboard.press('Tab')
    await page.keyboard.type('paypal')
    await page.keyboard.press('Tab')
    await page.keyboard.press('Tab')
    await page.keyboard.type('613154578')
    await page.keyboard.press('Tab')
    await page.keyboard.type('01/10/2001')
    await page.keyboard.press('Tab')
    await page.keyboard.type('Femmes')
    await page.keyboard.press('Tab')
    await page.keyboard.type('44')
    await page.keyboard.press('Tab')
    await page.keyboard.type('10 rue de Rue')
    await page.keyboard.press('Tab')
    await page.keyboard.press('Tab')
    await page.keyboard.type('Paris')
    await page.keyboard.press('Tab')
    await page.keyboard.type('75015')
    await page.waitFor(10000)
    await page.click('#mainresponce > div.row.m-0.bg-white.ptb-40 > div:nth-child(2) > div.row > div:nth-child(1) > div > label:nth-child(2)')
    await page.click('#preauth_tandc_email > label')
    await page.keyboard.press('Tab')
    await page.keyboard.press('Enter')
}

export async function launchPupForJo(subscription, page) {
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.email)
    await page.keyboard.press('Enter')
    await page.keyboard.press('Tab')
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.firstName)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.lastName)
    await page.keyboard.press('Tab')
    await page.keyboard.type(subscription.password)
    await page.click("#gigya-register-form > div:nth-child(2) > div.gigya-composite-control.gigya-composite-control-checkbox.privacyBox.gigya-terms-error > label")
    await page.keyboard.press('Enter')


}

async function checkVerificationMail(subscription, page) {
    puppeteer.launch({}).then(async browser => {
        const page = await browser.newPage()

        await page.goto("https://accounts.google.com/v3/signin/identifier?dsh=S-586371453%3A1669942073683980&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&rip=1&sacu=1&service=mail&flowName=GlifWebSignIn&flowEntry=ServiceLogin&ifkv=ARgdvAtFmL6NR05QjddUstSFCDp_tLPoJla03kY2cJNafTQrwSuEpSDevaRW-hkHhSHi7ehUzQrFOg")
        await page.keyboard.press('Tab')
        await page.keyboard.type(subscription.email)
        await page.keyboard.press('Enter')
        await page.keyboard.type(subscription.password)
        await page.keyboard.press('Enter')
        await page.keyboard.press('Tab')
        await page.keyboard.press('Tab')
        await page.keyboard.press('Tab')
        await page.keyboard.press('Tab')
        await page.keyboard.press('Tab')
        await page.keyboard.press('Enter')
        await page.close()
        await browser.close()
    })

}


function delay(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time)
    });
}

async function click(page, selector){
    await page.waitForSelector(selector)
    await page.click(selector)
}

async function type(page, selector, content){
    await page.waitForSelector(selector)
    await page.type(selector, content)
}