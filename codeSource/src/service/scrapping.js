import {transformCSVtoSubscriptions} from "./transformerCSVtoObject.js"
import {getProxies} from "./getProxies.js"
import {launchPup} from "./launchPup.js"
import {sendMessageOnDiscord} from "./sendMessageOnDiscord.js"

export async function scrapDataFromCsv(site, config) {
    const subscriptions = await transformCSVtoSubscriptions(site.pathOfCsv(), site.csvToObjectFunction)
    const proxies = await getProxies()
    await launchScrapping(site, subscriptions, proxies, config)
}

async function launchScrapping(site, subscriptions, proxies, config) {
    for (let i = 0; i < subscriptions.length; i++) {
        await launchPup(subscriptions[i].subscription, site, proxies[i], config)
        const nameOfArticle = "Raffle"
        if (config.useWebHook) {
            await sendMessageOnDiscord(site, subscriptions[i].subscription, proxies[i])
        }
    }
}