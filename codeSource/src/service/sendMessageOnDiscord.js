import {EmbedBuilder, WebhookClient} from "discord.js"
import config from "../../../config.js";

export async function sendMessageOnDiscord(site, subscription, proxy) {
    const nameOfArticle = getNameOfArticle(site.url)
    const nameOfSite = getNameOfSite(site.url)
    const embedToSend = getBasicEmbed(site.url, site.logoLinkToWebsite, nameOfSite)

    await site.discordMessageFunction(subscription, proxy, embedToSend, nameOfArticle)

    const webhookClient = new WebhookClient({url: config.webHookUrl})

    await webhookClient.send({
        username: 'Croissant',
        avatarURL: 'https://static.vecteezy.com/ti/vecteur-libre/p3/4335174-personnage-croissant-mignon-avec-emotions-heureuses-visage-sourire-yeux-bras-et-jambes-cuisine-gaie-personne-chignon-avec-expression-joyeuse-vector-flat-illustration-vectoriel.jpg',
        embeds: [embedToSend]
    })


}

function getBasicEmbed(url, logoLinkToWebsite, nameOfSite) {
    return new EmbedBuilder()
        .setColor('#0099ff')
        .setURL(url)
        .setThumbnail(logoLinkToWebsite)
        .addFields({name:"Site", value:nameOfSite, inline:false})
        .setTimestamp(new Date()
        );
}

function getNameOfSite(url) {
    return url.split("/")[2]
}

export async function addFieldsForHtmlTestPage(subscription, proxy, taskEmbed, nameOfArticle) {
    taskEmbed.addFields(
        {name: "fullName :", value: subscription.name, inline: true},
        {name: "proxy :", value: '||' + proxy + '||', inline: true},
        {name: "size :", value: subscription.size, inline: true},
        {name: "email:", value: subscription.email, inline: true}
    )
        .setTitle(nameOfArticle)
}

export async function addFieldsForStarcow(subscription, proxy, taskEmbed, nameOfArticle) {
    taskEmbed.addFields(
        {name: "nom de famille :", value: subscription.lastName, inline: true},
        {name: "prenom :", value: subscription.firstName, inline: true},
        {name: "proxy :", value: '||' + proxy + '||', inline: true},
        {name: "email:", value: subscription.email, inline: true},
        {name: "size:", value: subscription.size, inline: true},
        {name: "adress:", value: subscription.address, inline: true},
        {name: "code postal:", value: subscription.postalCode, inline: true},
        {name: "city:", value: subscription.city, inline: true},
        {name: "country:", value: subscription.country, inline: true},
        {name: "numéro de telephone:", value: subscription.phoneNumber, inline: true},
        {name: "instagram:", value: subscription.instagram, inline: true}
    )
        .setTitle(nameOfArticle)
}

export async function addFieldsForFootPatrol(subscription, proxy, taskEmbed, nameOfArticle) {
    taskEmbed.addFields(
        {name: "nom complet :", value: subscription.fullName, inline: true},
        {name: "proxy :", value: '||' + proxy + '||', inline: true},
        {name: "email:", value: subscription.email, inline: true},
        {name: "fullBirthday:", value: subscription.fullBirthday, inline: true},
        {name: "size:", value: subscription.sizeUE, inline: true},
        {name: "instagram:", value: subscription.instagram, inline: true},
        {name: "phoneNumber:", value: subscription.phoneNumber, inline: true},
        {name: "code postal:", value: subscription.postalCode, inline: true},
        {name: "city:", value: subscription.city, inline: true},
        {name: "country:", value: subscription.country, inline: true},
        {name: "numéro de telephone:", value: subscription.phoneNumber, inline: true},
        {name: "instagram:", value: subscription.instagram, inline: true}
    )
        .setTitle(nameOfArticle)
}

function getNameOfArticle(siteUrl) {
    return "nameOfArticle ouuu";
}