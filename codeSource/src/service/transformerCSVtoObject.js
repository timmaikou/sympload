import csvParser from "csv-parser"
import fs from "fs"

import {SubTest} from "../object/SubTest.js"
import {SubFootpatrol} from "../object/SubFootpatrol.js"
import {SubOpium} from "../object/SubOpium.js"
import {SubStarcow} from "../object/SubStarcow.js"
import {SubJo} from "../object/SubJo.js";

export function transformCSVtoSubscriptions(pathOfCsvFile, subToGet) {
    return new Promise((resolve, reject) => {
        const csv = csvParser
        let subscriptions = []

        fs.createReadStream(pathOfCsvFile)
            .pipe(csv())
            .on('data', (data) => {
                const subscription = subToGet(data)
                subscriptions.push({subscription})
            })
            .on('end', () => {
                resolve(subscriptions)
            })
            .on('error', reject)
    })
}

export function getSubFromHtmlTestPage(results) {
    const email = results.email
    const size = results.size
    const name = results.nom
    return new SubTest(name, email, size)
}

export function getSubFromStarcow(results) {
    const lastName = results.lastName
    const firstName = results.firstName
    const email = results.email
    const size = results.size
    const address = results.address
    const postalCode = results.postalCode
    const city = results.city
    const country = results.country
    const phoneNumber = results.phoneNumber
    const instagram = results.instagram
    return new SubStarcow(lastName, firstName, email, address, postalCode, city, country, phoneNumber, instagram, size)
}

export function getSubFromFootPatrol(results) {
    const fullBirthday = results.fullBirthday
    const fullBirthdaySplit = fullBirthday.split("/")
    const fullName = results.fullName
    const email = results.email
    const dayOfBirthday = fullBirthdaySplit[0]
    const monthOfBirthday = fullBirthdaySplit[1]
    const yearOfBirthday = fullBirthdaySplit[2]
    const instagram = results.instagram
    const phoneNumber = results.phoneNumber
    const city = results.city
    const postalCode = results.postalCode
    const sizeUE = results.sizeUE
    return new SubFootpatrol(fullName, email, dayOfBirthday, monthOfBirthday, yearOfBirthday, instagram, phoneNumber, city, postalCode, sizeUE, fullBirthday)
}

export function getSubFromJo(results) {
    const email = results.email
    const firstName = results.firstName
    const lastName = results.lastName
    const password = results.password
    return new SubJo(email, firstName, lastName, password)
}

function getBrokenarmSub(results) {
    let firstName = results.firstName
    let name = results.lastName
    const fullBirthdaySplit = results.fullBirthday.split("/")
    let dayOfBirthday = fullBirthdaySplit[0]
    let monthOfBirthday = fullBirthdaySplit[1]
    let yearOfBirthday = fullBirthdaySplit[2]
    let phone = results.phoneNumber
    let email = results.email
    let country = results.country
    let size = results.size
    return new SubBrokenarm(name, firstName, dayOfBirthday, monthOfBirthday, yearOfBirthday, phone, email, country, size)
}


function getOpiumSub(results) {
    let firstName = results.firstName
    let lastName = results.lastName
    let email = results.email
    let password = results.password
    let fullBirthday = results.fullBirthday
    let size = results.size
    return new SubOpium(firstName, lastName, email, password, fullBirthday, size)
}
